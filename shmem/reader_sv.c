#include <stdio.h>    // printf, getchar, fflush
#include <stdlib.h>   // atoi
#include <string.h>   // strcpy
#include <unistd.h>   // sleep
#include <sys/ipc.h>  // comunicacion entre procesos de SysV
#include <sys/shm.h>  // memoria compartida

int main(int argc, char *argv[])
{
    if (argc < 2) {
        puts("No hay suficientes argumentos\n");
        _exit(-1);
    }

    int shmid = atoi(argv[1]);

    void *shmaddr = shmat(shmid, NULL, SHM_RDONLY);
    printf("[READER] Segmento %d asociado a la dirección %X\n", shmid, shmaddr);
    fflush(NULL);

    int n = 0;
    printf("[READER] Leyendo...\n");
    do {
        n = printf("%s\n", shmaddr);
        fflush(NULL);
        sleep(1);
    } while (n < SHM_SIZE - 15);

    puts("[READER] Desasociando el segmento de memoria compartida");
    shmdt(shmaddr);
    puts("[READER] Fin");

    return 0;
}