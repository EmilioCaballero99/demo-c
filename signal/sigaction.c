#include<stdio.h>
#include<signal.h>
#include<unistd.h>
#include<stdlib.h>


void basic_handler(int signum){
    printf("\t\n Ya no sirvo debo morir :( %d\n", signum);
    exit(0);
}

void advance_handler(int signum, siginfo_t *siginfo, void *context){
    printf("\t\n [ADVANCE] Ya no sirvo debo morir :( %d\n", signum);
    printf("\t\n Enviado por PID: %d, UID: %d\n", siginfo->si_pid, siginfo->si_uid);
    exit(0);
}

int main(int argc, char *argv[])
{
    struct sigaction act = {0};

    printf("Mi PID es: %d\n", getpid());
    //act.sa_handler = &basic_handler;
    act.sa_sigaction = &advance_handler;

	act.sa_flags = SA_SIGINFO;

    sigaction(SIGINT, &act, NULL);

    while(1){
        printf("Estoy jackiando a la nasa :p\n");
        sleep(2);
    }

    return 0;
}
