#include<stdio.h>
#include<signal.h>
#include<unistd.h>
#include<stdlib.h>

void hup_hdl(int signum){
    printf("HIJO: Recibo SIGHUP\n");
}

void int_hdl(int signum){
    printf("HIJO: Recibo SIGINT\n");
}

void quit_hdl(int signum){
    printf("Mi padre me ha matado :(\n");
    exit(0);
}

int main(int argc, char *argv[])
{
    pid_t pid;

    pid = fork();

    if(pid == 0){/* Este es el hijo */
        signal(SIGHUP, hup_hdl);
        signal(SIGINT, int_hdl);
        signal(SIGQUIT, quit_hdl);
        while(1);
    }else{ /* Este es el padre*/
        printf("PADRE: Manda SIGHUP\n");
        kill(pid, SIGHUP);
        sleep(3);
        printf("PADRE: Manca C-c\n");
        kill(pid, SIGINT);
        sleep(3);
        printf("PADRE: Manda a matar al hijo\n");
        kill(pid, SIGQUIT);
        sleep(30);
    }
    return 0;
}

