#include<stdio.h>
#include<signal.h>
#include<unistd.h>
#include<stdlib.h>

void signal_handler(int signum){
  printf("\tAtrape la señal %d\n", signum);

  printf("\nAqui deberian limpiar y cerrar cosas\n");
}

int main(int argc, char *argv[]){
  pid_t pid = getpid();

  printf("PID: %d\n", pid);

  signal(SIGINT, signal_handler);
  if(signal(SIGUSR1, signal_handler) == SIG_ERR)
    printf("\nError que no debería ocurrir\n");
  if (signal(SIGKILL, signal_handler) == SIG_ERR)
    printf("\nError, no puedo manejar SIGKILL\n");
  if (signal(SIGSTOP, signal_handler) == SIG_ERR)
    printf("\nError, no puedo manejar SIGSTOP\n");



  while(1){
    printf("Haciando algo importante desde %d\n", pid);
    sleep(1);
  }

  return 0;
}
