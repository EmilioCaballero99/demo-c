#ifndef TYPES_H
#define TYPES_H

#include <sys/types.h>
#include <sys/socket.h>

struct sock {
    int fd;
    FILE *fp;
    struct sockaddr_in addr; // Dir y Puerto
};

#endif // TYPES_H