#ifndef ARGS_H
#define ARGS_H

#include "types.h"

/* 
 * handle_args - rellena la cfg con lo datos obtenidos desde la
 */
void handle_args(int argc, char const *argv[], struct sock *cfg);

#endif // ARGS_H