/* picoserver.c - Otro (muy) pequeño servidor web con fines educativos.
 * Esta implementación sirve archivos a peticiones GET. (Aun no)
 *
 * Emilio Cabrera <@emilio1625>
 * Diego Barriga <@umoqnier>
 *
 * Date: Nov. 2020
 */

// Tipos datos comunes que suelen devolver las llamadas a las bibliotecas en sys
#include <sys/types.h>
// funcionalidad de socket
#include <sys/socket.h>
// funciones relacionadas con conexiones ip
#include <netinet/ip.h>
// conversiones al byte order de la red
#include <arpa/inet.h>
// cerrar la conexion
#include <unistd.h>
#include <stdlib.h>
// Entrada estandar
#include <stdio.h>
// manipulacion de cadenas
#include <string.h>
// Obtener informacion del archivo
#include <sys/stat.h>  // stat(2)
#include <fcntl.h>

#include <sys/mman.h> // mmap

// tipos para nuestro codigo
#include "types.h"
// manejo de argumentos de la linea de comandos
#include "args.h"

int main(int argc, char const *argv[])
{
    struct sock server = {0};
    handle_args(argc, argv, &server);

    // man 7 socket
    server.fd = socket(AF_INET, SOCK_STREAM, 0);
    // ^ IPv4,    ^ Stream (TCP), ^ un solo protocolo en este socket
    // Manejo de error en la creación del socket
    if (server.fd == -1) {
        fprintf(stderr, "Error abriendo socket\n");
        exit(EXIT_FAILURE);
    }

    // configuramos el socket
    int optval = 1;
    if (setsockopt(server.fd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval)) == -1) {
        fprintf(stderr, "Error configurando el socket");
        // limpiando
        shutdown(server.fd, SHUT_WR);
        close(server.fd);
        exit(EXIT_FAILURE);
    }

    // solicitamos al sistema operativo la direccion y puerto
    if (bind(server.fd, (struct sockaddr *)&server.addr, sizeof(server.addr)) == -1) {
        fprintf(stderr, "Error asociando a la direccion %s puerto %d\n", inet_ntoa(server.addr.sin_addr),
                ntohs(server.addr.sin_port));
        // limpiando
        shutdown(server.fd, SHUT_WR);
        close(server.fd);
        exit(EXIT_FAILURE);
    }

    // escuchamos por nueva conexiones
    // man 2 listen
    if (listen(server.fd, 1) == -1) {  // bloquea
        fprintf(stderr, "Error al escuchar con el sockete\n");
        // limpiando
        shutdown(server.fd, SHUT_WR);
        close(server.fd);
        exit(EXIT_FAILURE);
    }

    // estructura para almacenar los datos del cliente
    // ver ip(7)
    struct sock client = {0};
    socklen_t addr_len = sizeof(client.addr);

    // creamos un nuevo socket para comunicarnos con el cliente
    // man 2 accept
    if ((client.fd = accept(server.fd, (struct sockaddr *)&client.addr, &addr_len)) == -1) {  // bloquea
        fprintf(stderr, "Error estableciendo conexion con el cliente\n");
        // limpiando
        shutdown(server.fd, SHUT_WR);
        close(server.fd);
        exit(EXIT_FAILURE);
    }

    fprintf(stderr, "Se establecio la conexion con %s en el puerto %d\n", inet_ntoa(client.addr.sin_addr),
            ntohs(client.addr.sin_port));

#define BUF_SIZE 1024
    char buf[BUF_SIZE];

    // Recibimos la peticion
    client.fp = fdopen(client.fd, "r+");  // convertimos el fd en un FILE *
    if (client.fp == NULL) {
        fputs("Error abriendo socket como stream", stderr);
        // limpiando
        shutdown(client.fd, SHUT_WR);
        close(client.fd);
        shutdown(server.fd, SHUT_WR);
        close(server.fd);
        exit(EXIT_FAILURE);
    }

    // Obtenemos el encabezado
    fgets(buf, BUF_SIZE, client.fp);
    fputs(buf, stderr);
    // Obtenemos el metodo, la uri y la version
    char method[32], path[256], version[32];
    sscanf(buf, "%s %s %s\n", method, path, version);
    fprintf(stderr, "%s", method);

    if (strcmp(method, "GET")) {
        // TODO: crear y reemplazar esto por una fucion que genere errores y otra
        // que los envie al cliente
        fputs("HTTP/1.1 501 Not Implemented\r\n\r\n", client.fp);
        fflush(client.fp)
    }

    char *needle = strstr(path, "?");
    if (needle != NULL) {  // descartamos peticiones GET
        *needle = '\0';
    }

    char *filename = (path + 1);  // descartamos el slash inicial
    if (*filename == '\0') {      // si la cadena es vacia enviamos un archivo por defecto
        strcpy(filename, "index.html");
    }

    // verificamos que el archivo exista
    struct stat fileinfo;
    // TODO: investigar como usar stat para obtener info del archivo
    if (/* TODO: verificar si el archivo existe */) {
        // TODO: manejar errores enviar 404 usando la funcion para enviar errores
    }

    if (/* TODO: verificar que el archivo sea un archivo regular */) {
        // TODO: manejar el error, de la siguente manera
        // * añadir index.html al nombre del archivo y probar de nuevo
        // * si lo anterior no funciona, enviar un error de HTTP
    }

    // abrimos el archivo y mapeamos el contenido a memoria
    int filefd    = open(filename, O_RDONLY);
    char *filemap = mmap(NULL, fileinfo.st_size, PROT_READ, MAP_PRIVATE, filefd, 0);

    // TODO: determinar el MIME Type del archivo segun la extension 
    // TODO: crear una funcion que genere respuestas de HTTP
    // y envie el contenido del archivo 

    // limpiando
    shutdown(client.fd, SHUT_WR);
    fclose(client.fp);  // cierra tambien el file descriptor
    // close(client.fd);
    shutdown(server.fd, SHUT_WR);
    close(server.fd);

    return 0;
}