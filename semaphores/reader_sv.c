#include <stdio.h>    // printf, getchar, fflush
#include <stdlib.h>   // atoi
#include <string.h>   // strcpy
#include <unistd.h>   // sleep
#include <sys/ipc.h>  // comunicacion entre procesos de SysV
#include <sys/shm.h>  // memoria compartida
#include <sys/sem.h>  //semaforos

#define SHM_SIZE 128

int main(int argc, char *argv[])
{
    if (argc < 3) {
        puts("No hay suficientes argumentos\n");
        _exit(-1);
    }

    int shmid = atoi(argv[1]);

    void *shmaddr = shmat(shmid, NULL, SHM_RDONLY);
    printf("[READER] Segmento %d asociado a la dirección %X\n", shmid, shmaddr);

    // obtenemos el semaforo
    int semid = atoi(argv[2]);

    int n = 0;
    printf("[READER] Leyendo...\n");
    fflush(NULL);
    struct sembuf op = {0};
    do {
        //esperamos antes de intentar imprimir
        op.sem_num = 0;
        op.sem_op = 0;
        op.sem_flg = 0;
        semop(semid, &op, 1);
        n = printf("%s\n", shmaddr);
        fflush(NULL);
    } while (n < SHM_SIZE - 15);

    puts("[READER] Desasociando el segmento de memoria compartida");
    shmdt(shmaddr);
    puts("[READER] Fin");

    return 0;
}