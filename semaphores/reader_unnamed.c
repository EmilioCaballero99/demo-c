#include <fcntl.h>
#include <stdio.h>     // printf, getchar, fflush
#include <stdlib.h>    // atoi
#include <string.h>    // strcpy
#include <sys/mman.h>  // mmap
#include <unistd.h>    // sleep
#include <semaphore.h>

#define SHM_SIZE 128
#define SHM_NAME "/myshmem"
#define SEM_NAME "/mysemaforo"

int main(int argc, char *argv[])
{
    int fd = shm_open(SHM_NAME, O_RDWR, 0660);
    puts("[READER] Abriendo el segmento de memoria compartida");

    void *shmaddr = mmap(NULL, SHM_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    printf("[READER] Segmento asociado a la dirección %X\n", shmaddr);
    fflush(NULL);

    // sem_t *sem = sem_open(SEM_NAME, O_RDWR);
    sem_t *sem = shmaddr;

    int n = 0;
    printf("[READER] Leyendo...\n");
    do {
        sem_wait(sem);
        n = printf("%s\n", shmaddr + sizeof(*sem));
        fflush(NULL);
        // sleep(1);
    } while (n < SHM_SIZE - sizeof(*sem) - 15);

    puts("[READER] Desasociando el segmento de memoria compartida");
    munmap(shmaddr, SHM_SIZE);
    close(fd);

    // sem_close(sem);
    puts("[READER] Fin");

    return 0;
}