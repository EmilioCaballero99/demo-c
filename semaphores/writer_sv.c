#include <stdio.h>    // printf, getchar, fflush
#include <string.h>   // strcpy
#include <unistd.h>   // sleep
#include <sys/ipc.h>  // comunicacion entre procesos de SysV
#include <sys/shm.h>  // memoria compartida
#include <sys/sem.h>  // semaforos

#define SHM_SIZE 128

union semun {              // union para el 4to argumento de semctl
    int val;               /* Value for SETVAL */
    struct semid_ds *buf;  /* Buffer for IPC_STAT, IPC_SET */
    unsigned short *array; /* Array for GETALL, SETALL */
    struct seminfo *__buf; /* Buffer for IPC_INFO (Linux-specific) */
};

int main(int argc, char *argv[])
{
    // creando un nuevo segmento de memoria compartida
    int shmid = shmget(IPC_PRIVATE, SHM_SIZE, IPC_CREAT | IPC_EXCL | 0660);
    printf("[WRITER] Nuevo segmento de memoria creado: %d\n", shmid);

    // mapeando ese segmento a la memoria del programa
    void *shmaddr = shmat(shmid, NULL, SHM_R | SHM_W);
    printf("[WRITER] Segmento %d asociado a la direccion: %X\n", shmid, shmaddr);

    // creando un semaforo para sincronizar el acceso
    int semid = semget(IPC_PRIVATE, 1, IPC_CREAT | IPC_EXCL | 0660);
    // inicializando el semaforo
    union semun semun = {.val = -1}; // valor en el que vamos a inicializar el semaforo
    semctl(semid, 0, SETVAL, semun);
    printf("[WRITER] Nuevo semaforo: %d\n", semid);
    getchar();

    printf("[WRITER] Escribiendo...\n");
    fflush(NULL);

    struct sembuf op = {0};
    for (int offset = 0; offset < SHM_SIZE; offset += 12) {
        strncpy(shmaddr + offset, "hola mundo! ", 13);
        sleep(1);
        // sem.signal
        op.sem_num = 0;
        op.sem_op = 1;
        op.sem_flg = 0;
        semop(semid, &op, 1);
    }

    puts("[WRITER] Desasociando el segmento de memoria compartida");
    shmdt(shmaddr);
    puts("[WRITER] Marcando el segmento para ser eliminado");
    // shmctl(shmid, IPC_RMID, NULL);
    puts("[WRITER] fin\n");
    fflush(NULL);

    getchar();

    return 0;
}