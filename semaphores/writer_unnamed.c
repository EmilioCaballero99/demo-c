#include <stdio.h>     // printf, getchar, fflush
#include <string.h>    // strcpy
#include <unistd.h>    // sleep
#include <sys/stat.h>  // constantes
#include <fcntl.h>     // control de archivos
#include <sys/mman.h>  // manejo de memoria (mmap)
#include <semaphore.h>

#define SHM_SIZE 128
#define SHM_NAME "/myshmem"
// #define SEM_NAME "/mysemaforo"

int main(int argc, char *argv[])
{
    shm_unlink(SHM_NAME);
    int fd = shm_open(SHM_NAME, O_RDWR|O_CREAT, 0660);
    printf("[WRITER] Nuevo segmento de memoria creado: %d\n", fd);
    
    ftruncate(fd, 0);
    ftruncate(fd, SHM_SIZE);
    puts("[WRITER] Segmento redimensionado");
    
    void *shmaddr = mmap(NULL, SHM_SIZE, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
    printf("[WRITER] Segmento %d mapeado a la direccion: %X\n", fd, shmaddr);
    
    // sem_unlink(SEM_NAME);
    // sem_t *sem = sem_open(SEM_NAME, O_CREAT | O_RDWR, 0660, 0);
    sem_t *sem = shmaddr;
    sem_init(sem, -1, 0);
    printf("[WRITER] Nuevo semaforo al inicio de la memoria compartida\n");
    fflush(NULL);
    getchar();

    printf("[WRITER] Escribiendo...\n");
    fflush(NULL);
    for (int offset = sizeof(* sem); offset < SHM_SIZE; offset += 12) {
        strncpy(shmaddr + offset, "hola mundo! ", 13);
        sem_post(sem);
        sleep(1);
    }

    puts("[WRITER] Desmapeando el segmento de memoria compartida");
    munmap(shmaddr, SHM_SIZE);
    puts("[WRITER] Marcando el segmento para ser eliminado");
    shm_unlink(SHM_NAME);
    close(fd);
    puts("[WRITER] Fin\n");
    fflush(NULL);

    // sem_close(sem);
    //sem_unlink(SEM_NAME);

    return 0;
}