#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/wait.h>
#include <semaphore.h>

// Rendezvous: Es un punto de encuentro entro dos procesos ninguno de los dos
// puede continuar hasta que el otro llegue al punto de encuentro

int main()
{
    // Creamos un nuevo semaforo en memoria
    sem_t *waitA = mmap(NULL, 2 * sizeof(sem_t), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANON, -1, 0);
    sem_t *waitB = waitA + sizeof(sem_t);
    sem_init(waitA, 1, 0);
    sem_init(waitB, 1, 0);

    pid_t pid = fork();
    if (pid < 0) {
        perror("Padre");
    } else if (pid == 0) {  // Primer hijo hilo A
        printf("eeny, ");
        fflush(NULL);
        sem_post(waitB);
        sem_wait(waitA);
        printf("miny, ");
        fflush(NULL);
        _exit(0);
    } else {
        pid = fork();
        if (pid < 0) {
            perror("Padre");
        } else if (pid > 0) {
            wait(NULL);
        } else {  // Segundo hijo hilo B
            printf("meeny, ");
            fflush(NULL);
            sem_post(waitA);
            sem_wait(waitB);
            printf("moe, ");
            fflush(NULL);
            _exit(0);
        }
        wait(NULL);
        sem_destroy(waitA);
        sem_destroy(waitB);
        munmap(waitA, 2 * sizeof(sem_t));
        puts("Adiós");
    }
}