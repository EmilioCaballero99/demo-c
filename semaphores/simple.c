#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/wait.h>
#include <semaphore.h>

// Signaling: es un patron que sincroniza a dos (hilos/procesos), normalmente se
// usa para indicar que a ocurrido un evento 

int main()
{
    // Creamos un nuevo semaforo en memoria
    sem_t *sem = mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANON, -1, 0);
    sem_init(sem, 1, 0);

    pid_t pid = fork();
    if (pid < 0) {
        perror("Padre");
    } else if (pid == 0) {  // Primer hijo Hilo B
        // sem_wait(sem);
        puts(" cruel :c");
        fflush(NULL);
        _exit(0);
    } else {
        pid = fork();
        if (pid < 0) {
            perror("Padre");
        } else if (pid > 0) {
            wait(NULL);
        } else {  // Segundo hijo Hilo A
            printf("Hola mundo");
            fflush(NULL);
            // sem_post(sem);
            _exit(0);
        }
        wait(NULL);
        sem_destroy(sem);
        munmap(sem, sizeof(sem));
        puts("Adiós");
    }
}