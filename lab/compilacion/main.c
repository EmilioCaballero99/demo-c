#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef FIB_ITER
    #include "fib/fib_iter.h"
#else
    #include "fib/fib.h"
#endif

int main(int argc, char const *argv[])
{
    unsigned int n;
    // Procesamos los argumentos de la línea de comandos
    if (argc == 3 && strcmp(argv[1], "-n") == 0) {
        int tmp = atoi(argv[2]);
        if (tmp < 0) {
            return -1;
        }
        n = (unsigned int)tmp;
    } else {
        return 1;
    }

    // creamos un nuevo arreglo del tamaño solicitado
    unsigned int *values;
    values = malloc(n * sizeof(* values));

    // llamamos a nuestra biblioteca
    fib(values, n);

    // impl
    for (size_t i = 0; i < n; i++)
    {
        printf("%u, ", *(values + i));
    }
    printf("\n");
    
    return 0;
}
 