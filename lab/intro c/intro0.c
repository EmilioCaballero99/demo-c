// @author: Emilio Cabrera <emilio1625@ciencias.unam.mx>

// Bibliotecas
#include <stdio.h>

// variables globales

// declarada
int b; // 64 bits
// inicializada
unsigned int a = 0; // 64 bits
float f; // 32 bits
double d; // 64bits

// main sencillo
main()
{
    int c;

    char ch = 'A';

    // char str[5];
    char str[] = {'H', 'o', 'l', 'a', '\0'};

    // int iarr[6];
    int iarr[] = {0, 1, 2, 3};

    printf("Hola mundo!");
    /* Sintaxis de estructuras de control
    for (sent sent sent) {
        break;
        sentencias
    }

    if (expresion) {
        sentencias
    } else {
        sentencias
    }

    while (expre) {
        break
        sentencias
    }

    switch (expre) {
        case constantes numerica:
        case const:
        default:
    }
    */
}

// Definicion de función
int suma(int a, int b) {
    return a+b;
}
