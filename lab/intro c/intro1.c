// @author: Emilio Cabrera <emilio1625@ciencias.unam.mx>

// Main estandar
#include <stdio.h>
#include <string.h>

int main(int argc, char const *argv[])
{
    printf("Total argumentos: %d\n", argc);
    for (int i = 0; i < argc; i++)
    {
        printf("Argumento %d: %s\n", i, argv[i]);
    }

    if (strcmp(argv[0], "./intro1") == 0) {
        printf("hola\n");
    } else {
        printf("adios\n");
    }

    return 0;
}
