/* hello-char - Módulo del kernel que crea un dispositivo de caracteres que
 * al ser leído dice "Hola ".
 * Para probarlo:
 * 1. Compila este código ejecutando `make` en la carpeta.
 * 2. Inserta el modulo en el kernel
 *     $ sudo insmod ./hello-char.ko 
 * 3. Copia el número mayor que se imprime en el log del kernel
 *     $ dmesg | tail
 * 4. Usando el número crea un nuevo archivo en /dev
 *     $ sudo mknod /dev/hello-char c <numero_mayor> 0
 * 5. Usa `cat` para leer del archivo
 *     $ cat /dev/hello-char
 */



#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/fs.h>
#include <linux/uaccess.h>

// Nombre del módulo y el archivo en /dev asociado a este
#define DEV_NAME "hello-char"

/* device_open - informa al kernel que se abrió el archivo asociado al driver
 * y evita de esta forma que el modulo sea eliminado del kernel usando rmmod
 * mientras el archivo este abierto.
 * NOTA: Copia la firma (tipo de retorno y el tipo de parametros) para tú 
 * practica.
 */
static int device_open (struct inode *i, struct file *f) {
    printk(KERN_INFO DEV_NAME ": Dispositivo abierto");
    // Esta función informa al kernel que se esta usando el módulo, por cada una
    // de las llamadas a esta función debe haber una llamada a module_put
    try_module_get(THIS_MODULE);
    return 0;
}

/* device_release -  informa al kernel que el archivo asociado al driver ya no
 * esta en uso.
 * NOTA: Copia la firma (tipo de retorno y el tipo de parametros) para tú 
 * practica.
 */
static int device_release(struct inode *i, struct file *f) {
    printk(KERN_INFO DEV_NAME ": Dispositivo cerrado");
    module_put(THIS_MODULE);
    return 0;
}

// Mensaje que se copiará en el buffer del usuario
static char *msg = "Hola ";

/* device_read - Copia el mensaje a un buffer en espacio de usuario.
 * NOTA: La implementación de esta función en tu práctica debe cuidar que no se
 * copien más bytes de los indicados por el parámetro count. Copia la firma de 
 * la función (tipo de retorno y el tipo de parametros) para tú practica.
 */
static ssize_t device_read(struct file *file, char __user *buf, size_t count, loff_t *o) {
    int res = count;
    while (res > 6) {
        copy_to_user(buf, msg, 6);
        res -= 6;
    }
    return count - res;
}

/* device_write - Devuelve error al intentar abrir el archivo asociado a este 
 * modulo para escritura.
 * NOTA: Copia la firma (tipo de retorno y el tipo de parametros) para tú 
 * practica.
 */
ssize_t device_write(struct file *f, const char *c, size_t s, loff_t *o) {
    return -EINVAL;
}

// Operaciones soportadas por el archivo en /dev 
static struct file_operations fops = {
    .owner = THIS_MODULE,
    .open = device_open,
    .release = device_release,
    .write = device_write,
    .read = device_read
};

// Numero de dispositivo asociado al archivo en /dev
static int major;

/* inicio - Registra un nuevo dispositivo de caracteres al cargar el modulo en
 * kernel.
 */
int __init inicio(void)
{
    // Registramos un nuevo dispositivo de caracteres en el kernel
    major = register_chrdev(0, DEV_NAME, &fops);

    if (major < 0) {
        printk(KERN_ALERT DEV_NAME ": Error al registrar un nuevo dispositivo");
        return major;
    }

    // Si no ves esta linea en el log, intenta cambiar la prioridad de KERN_INFO
    // a KERN_NOTICE
    printk(KERN_INFO DEV_NAME ": Numero mayor: %d", major);
    return 0;
}

/* fin - Elimina el dispositivo de caracteres previamente creado. */
void __exit fin(void) {
    if (major > 0) {
        printk(KERN_INFO DEV_NAME ": Eliminando el dispositivo");
        unregister_chrdev(major, DEV_NAME);
    }
}

// Registramos las funciones de inicialización y término del módulo
module_init(inicio);
module_exit(fin);

// Registramos la licencia, autor y descripción del módulo
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Emilio Cabrera");
MODULE_DESCRIPTION("Modulo de prueba");